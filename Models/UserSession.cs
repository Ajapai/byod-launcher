﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ByodLauncher.Models
{
	public class UserSession
	{
		public Guid Id { get; set; }
		public Guid DirectorId { get; set; }
		public Director Director { get; set; }
		public string Cookie { get; set; }
		public DateTime ExpirationDate { get; set; }
	}
}
