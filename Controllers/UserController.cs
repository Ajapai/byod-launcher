using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ByodLauncher.Models;

namespace ByodLauncher.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ByodLauncherContext _context;

        public UserController(ByodLauncherContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetDirector()
        {
            UserSession userSession = await _context.UserSessions.FirstAsync(s => s.Id.ToString() == Request.Cookies["UserSession"]);
            Director director = await _context.Directors.FirstAsync(u => u.Id == userSession.DirectorId);
            return Ok(director.Firstname + " " + director.Name);
        }

        [HttpPost]
        public async Task<IActionResult> CheckForUser(
            [FromHeader(Name = "X-First")] string firstName, 
            [FromHeader(Name = "X-Last")] string lastName, 
            [FromHeader(Name = "X-UserId")] Guid directorId)
        {
            // Create User and UserSession if User does not exist
            if(!DirectorExists(directorId))
            {
                Director director = new Director(){Firstname = firstName, Name = lastName, Id = directorId};
                await _context.Directors.AddAsync(director);

                // Generate an unused Guid for the UserSession
                Guid guid = Guid.NewGuid();
                while(await _context.UserSessions.AnyAsync(s => s.Id == guid))
                {
                    guid = Guid.NewGuid();
                }
                await _context.UserSessions.AddAsync(new UserSession()
                {
                    Id = guid, 
                    DirectorId = directorId, 
                    Director = director,
                    Cookie = "Not Sure what to put here",
                    ExpirationDate = DateTime.Now.AddHours(12)
                });

                await _context.SaveChangesAsync();
            }

            // Extend UserSession if needed
            if(!UserSessionIsActive(directorId))
            {
                UserSession uSession = await _context.UserSessions.FirstAsync(u => u.DirectorId == directorId);
                uSession.ExpirationDate = DateTime.Now.AddHours(12);
                await _context.SaveChangesAsync();
            }
            
            UserSession userSession = await _context.UserSessions.FirstAsync(u => u.DirectorId == directorId);
            return Redirect($"/api/Authentication/SetCookie?SessionId={userSession.Id}");
        }

        
        private bool DirectorExists(Guid directorId)
        {
            return _context.Directors.Any(u => u.Id == directorId);
        }

        private bool UserSessionIsActive(Guid directorId)
        {
            return _context.UserSessions.First(s => s.DirectorId == directorId).ExpirationDate > DateTime.Now;
        }
    }
}