using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ByodLauncher.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ByodLauncher.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ByodLauncherContext _context;

        public AuthenticationController(IConfiguration configuration, ByodLauncherContext context)
        {
            _configuration = configuration;
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Login([FromQuery] string username, [FromQuery] string authToken)
        {
            var expectedAuthToken = _configuration["Authentication:DummyAuthenticationToken"];
            if (!string.IsNullOrEmpty(username) && expectedAuthToken.Equals(authToken))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, username)
                };
                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(new[] {claimsIdentity});
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, claimsPrincipal);
            }
            else
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpGet("SetCookie")]
        public async Task<IActionResult> SetCookie([FromQuery] string SessionId)
        {
            UserSession uSession = await _context.UserSessions.FirstAsync(session => session.Id == Guid.Parse(SessionId));
            if (uSession == null)
            {
                return BadRequest();
            }
            Response.Cookies.Append("UserSession", SessionId, new CookieOptions() { Expires = uSession.ExpirationDate });
            return Ok();
        }
    }
}