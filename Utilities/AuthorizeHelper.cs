﻿using ByodLauncher.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ByodLauncher.Utilities
{
    public static class AuthorizeHelper
    {
        private static string GetSessionId(HttpContext httpContext)
        {
            return httpContext.Request.Cookies["UserSession"];
        }

        public static Director GetAuthorizedUser(HttpContext httpContext, ByodLauncherContext context)
        {
            if (IsAuthorizedRequest(httpContext,context))
            {
                var userSession = context.UserSessions.Find(Guid.Parse(GetSessionId(httpContext)));
                return context.Directors.Find(userSession.DirectorId);
            }
            return null;
        }

        public static bool IsAuthorizedRequest(HttpContext httpContext, ByodLauncherContext context)
        {
            var sessionId = GetSessionId(httpContext);
            if (String.IsNullOrEmpty(sessionId))
            {
                return false;
            }
            var userSession = context.UserSessions.Find(Guid.Parse(sessionId));
            if (userSession != null && userSession.ExpirationDate > DateTime.Now)
            {
                return true;
            }
            return false;
        }
    }
}
