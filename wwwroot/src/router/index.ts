import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NewSession from '../views/NewSession.vue'
import NewTutorialTarget from '../views/NewTutorialTarget.vue'
import JoinSession from '../views/JoinSession.vue'
import AttendSession from '../views/AttendSession.vue'
import OrchestrateSession from "@/views/OrchestrateSession.vue";
import EditSession from "@/views/EditSession.vue";
import Login from "@/views/Login.vue";
import SessionList from "@/components/SessionList.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/new-session',
        name: 'new session',
        component: NewSession
    },
    {
        //versuch dynamisches routing, edit-code in url 
        path: '/edit-session/:editcode',
        name: 'edit session',
        component: EditSession,
        props: true
    },
    {
        path: '/new-tutorial-target',
        name: 'new tutorial target',
        component: NewTutorialTarget
    },
    {
        path: '/join-session/:participantId',
        name: 'join session',
        component: JoinSession,
        props: true
    },
    {
        path: '/orchestrate-session',
        name: 'orchestrate session',
        component: OrchestrateSession,
    },
    {
        path: '/attend-session',
        name: 'attend session',
        component: AttendSession,
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/my-sessions',
        name: 'my sessions',
        component: SessionList
    }
    // {
    //   path: '/about',
    //   name: 'About',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

// router.beforeEach((to, from, next) => {
//     if (to.name == 'new session' && !isSessionCookieSet()) next('/')
//     else if (to.name == 'edit session' && !isSessionCookieSet()) next('/')
//     else if (to.name == 'orchestrate session' && !isSessionCookieSet()) next('/')
//         //all routes which one should authorized add admin page
//     else next()
// })

export function isSessionCookieSet() {
    const value = "; " + document.cookie;
    const parts = value.split("; " + "UserSession" + "=");

    if (parts.length == 2) {
        return true
    }
    return false
}

router.beforeEach((to, from, next) => {
    if (to.name == 'new session' && !isSessionCookieSet()) next('/')
    else if (to.name == 'edit session' && !isSessionCookieSet()) next('/')
    else if (to.name == 'orchestrate session' && !isSessionCookieSet()) next('/')
    else if (to.name == 'login' && !isSessionCookieSet()) next('/')
        //all routes which one should authorized add admin page
    else next()
})

export default router
